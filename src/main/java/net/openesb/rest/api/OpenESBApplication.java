package net.openesb.rest.api;

import net.openesb.rest.api.provider.ComponentNotFoundExceptionMapper;
import net.openesb.rest.api.provider.CrossDomainFilter;
import net.openesb.rest.api.provider.ManagementExceptionMapper;
import net.openesb.rest.api.provider.ObjectMapperProvider;
import net.openesb.rest.api.provider.ServiceInjectionProvider;
import net.openesb.rest.api.resources.ComponentsResource;
import net.openesb.rest.api.resources.InstanceResource;
import net.openesb.rest.api.resources.JVMInformationsResource;
import net.openesb.rest.api.resources.MessageServiceResource;
import net.openesb.rest.api.resources.ServiceAssembliesResource;
import net.openesb.rest.api.resources.SharedLibrariesResource;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class OpenESBApplication extends ResourceConfig {

    public OpenESBApplication() {
        super(
                // Register Root Resources
                InstanceResource.class,
                ServiceAssembliesResource.class,
                ComponentsResource.class,
                SharedLibrariesResource.class,
                MessageServiceResource.class,
                JVMInformationsResource.class,
                
                // Register Exception mapper
                ManagementExceptionMapper.class,
                ComponentNotFoundExceptionMapper.class,
                
                // Register filter
            //    BasicAuthenticationFilter.class,
                CrossDomainFilter.class,
                
                // Register Jackson ObjectMapper resolver
                ObjectMapperProvider.class,
                JacksonFeature.class,
                MultiPartFeature.class);
        
        register(new ServiceInjectionProvider());
    }
}
