package net.openesb.rest.api.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.Instance;
import net.openesb.management.api.AdministrationService;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("/")
public class InstanceResource extends AbstractResource {
    
    @Context
    private ResourceContext resourceContext;
    
    @Context
    private AdministrationService administrationService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Instance getInformations() throws ManagementException {
        return administrationService.getInstance();
    }
    
    @Path("loggers")
    public InstanceLoggersResource getInstanceLoggersResource() {
        return resourceContext.initResource(
                new InstanceLoggersResource());
    }
}
