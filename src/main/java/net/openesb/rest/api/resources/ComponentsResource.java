package net.openesb.rest.api.resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.openesb.management.api.ComponentType;
import net.openesb.model.api.JBIComponent;
import net.openesb.management.api.ComponentService;
import net.openesb.management.api.ManagementException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("/components")
public class ComponentsResource extends AbstractResource {

    @Context
    private ComponentService componentService;
    
    @Context
    private ResourceContext resourceContext;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<JBIComponent> listComponents(
            @QueryParam("type") String type,
            @QueryParam("state") String state,
            @QueryParam("library") String sharedLibraryName,
            @QueryParam("assembly") String serviceAssemblyName)
            throws ManagementException {

        ComponentType componentType = ComponentType.BINDINGS_AND_ENGINES;

        if (type != null && type.trim().length() > 0) {
            if (type.equalsIgnoreCase(ComponentType.BINDING.name())) {
                componentType = ComponentType.BINDING;
            } else if (type.equalsIgnoreCase(ComponentType.ENGINE.name())) {
                componentType = ComponentType.ENGINE;
            }
        }

        Set<JBIComponent> components = new TreeSet<JBIComponent>(new Comparator<JBIComponent>() {
            @Override
            public int compare(JBIComponent comp1, JBIComponent comp2) {
                return comp1.getName().compareTo(comp2.getName());
            }
        });

        components.addAll(componentService.findComponents(
                componentType,
                state,
                sharedLibraryName,
                serviceAssemblyName));

        return components;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response install(@FormDataParam("component") InputStream is,
            @FormDataParam("component") FormDataContentDisposition fileDisposition) throws ManagementException {

        File compArchive = null;

        try {
            compArchive = createTemporaryFile(is, fileDisposition.getFileName());
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "I/O errors while uploading the component archive.", e);
            return Response.serverError().build();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    getLogger().log(Level.SEVERE, null, ex);
                }
            }
        }

        if (compArchive != null) {
            String componentName = componentService.install(compArchive.toURI().toString());
            return Response.ok().entity(componentName).build();
        }

        return Response.serverError().build();
    }

    @Path("{component}")
    public ComponentResource getComponentResource(@PathParam("component") String componentName) {
        return resourceContext.initResource(
                new ComponentResource(componentName));
    }
}
