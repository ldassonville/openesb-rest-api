package net.openesb.rest.api.resources;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("jvm")
public class JVMInformationsResource {
    
    private final RuntimeMXBean mxBean;
    
    public JVMInformationsResource() {
        this.mxBean = ManagementFactory.getRuntimeMXBean();
    }
    
    @GET
    public Map<String, Object> getInformations() {
        final Map<String, Object> infos = new HashMap<String, Object>();
        
        infos.put("name", mxBean.getName());
        infos.put("spec.name", mxBean.getSpecName());
        infos.put("spec.vendor", mxBean.getSpecVendor());
        infos.put("spec.version", mxBean.getSpecVersion());
        infos.put("vm.name", mxBean.getVmName());
        infos.put("vm.vendor", mxBean.getVmVendor());
        infos.put("vm.version", mxBean.getVmVersion());
        infos.put("mgmt.spec.version", mxBean.getManagementSpecVersion());
        infos.put("system.properties", mxBean.getSystemProperties());
        
        return Collections.unmodifiableMap(infos);
    }
    
    @Path("gc")
    public GarbageCollectorMetricsResource getGarbageCollectorMetricsResource() {
        return new GarbageCollectorMetricsResource();
    }
    
    @Path("memory")
    public MemoryUsageMetricsResource getMemoryUsageMetricsResource() {
        return new MemoryUsageMetricsResource();
    }
    
    @Path("thread")
    public ThreadStatesMetricsResource getThreadStatesMetricsResource() {
        return new ThreadStatesMetricsResource();
    }
}
