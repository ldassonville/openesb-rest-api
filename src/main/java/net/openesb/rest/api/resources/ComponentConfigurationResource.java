package net.openesb.rest.api.resources;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.ComponentConfiguration;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentConfigurationResource extends AbstractResource {
    
    @Context
    private ConfigurationService configurationService;
    
    private final String componentName;
    
    public ComponentConfigurationResource(String componentName) {
        this.componentName = componentName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<ComponentConfiguration> getComponentConfiguration() throws ManagementException {
        Set<ComponentConfiguration> configurations = new TreeSet<ComponentConfiguration>(new Comparator<ComponentConfiguration>() {
            @Override
            public int compare(ComponentConfiguration conf1, ComponentConfiguration conf2) {
                return conf1.getName().compareTo(conf2.getName());
            }
        });

        configurations.addAll(configurationService.
                getComponentConfiguration(componentName));
        
        return configurations;
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<ComponentConfiguration> setComponentConfiguration(Set<ComponentConfiguration> configurations) throws ManagementException {
        configurationService.updateComponentConfiguration(
                componentName, configurations);
        
        return getComponentConfiguration();
    }
}
