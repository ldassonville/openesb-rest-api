package net.openesb.rest.api.resources;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.GET;
import net.openesb.model.api.metric.Gauge;
import net.openesb.model.api.metric.Metric;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ThreadStatesMetricsResource extends AbstractResource {
    
    // do not compute stack traces.
    private final static int STACK_TRACE_DEPTH = 0;
    private static final int MAX_STACK_TRACE_DEPTH = 100;
    
    private final ThreadMXBean threads;
    
    public ThreadStatesMetricsResource() {
        this.threads = ManagementFactory.getThreadMXBean();
    }
    
    @GET
    public Map<String, Metric> getMetrics() {
        final Map<String, Metric> gauges = new HashMap<String, Metric>();

        for (final Thread.State state : Thread.State.values()) {
            gauges.put(name(state.toString().toLowerCase(), "count"),
                       new Gauge<Object>() {
                           @Override
                           public Object getValue() {
                               return getThreadCount(state);
                           }
                       });
        }

        gauges.put("count", new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return threads.getThreadCount();
            }
        });

        gauges.put("daemon.count", new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return threads.getDaemonThreadCount();
            }
        });

        gauges.put("deadlock.count", new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return getDeadlockedThreads().size();
            }
        });

        gauges.put("deadlocks", new Gauge<Set<String>>() {
            @Override
            public Set<String> getValue() {
                return getDeadlockedThreads();
            }
        });

        return Collections.unmodifiableMap(gauges);
    }

    private int getThreadCount(Thread.State state) {
        final ThreadInfo[] allThreads = getThreadInfo();
        int count = 0;
        for (ThreadInfo info : allThreads) {
            if (info != null && info.getThreadState() == state) {
                count++;
            }
        }
        return count;
    }

    ThreadInfo[] getThreadInfo() {
        return threads.getThreadInfo(threads.getAllThreadIds(), STACK_TRACE_DEPTH);
    }
    
    /**
     * Returns a set of diagnostic stack traces for any deadlocked threads. If no threads are
     * deadlocked, returns an empty set.
     *
     * @return stack traces for deadlocked threads or an empty set
     */
    private Set<String> getDeadlockedThreads() {
        final long[] ids = threads.findDeadlockedThreads();
        if (ids != null) {
            final Set<String> deadlocks = new HashSet<String>();
            for (ThreadInfo info : threads.getThreadInfo(ids, MAX_STACK_TRACE_DEPTH)) {
                final StringBuilder stackTrace = new StringBuilder();
                for (StackTraceElement element : info.getStackTrace()) {
                    stackTrace.append("\t at ")
                              .append(element.toString())
                              .append(String.format("%n"));
                }

                deadlocks.add(
                        String.format("%s locked on %s (owned by %s):%n%s",
                                      info.getThreadName(),
                                      info.getLockName(),
                                      info.getLockOwnerName(),
                                      stackTrace.toString()
                        )
                );
            }
            return Collections.unmodifiableSet(deadlocks);
        }
        return Collections.emptySet();
    }
}
