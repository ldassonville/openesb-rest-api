package net.openesb.rest.api.resources;

import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.NMR;
import net.openesb.model.api.metric.Metric;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.MessageService;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("nmr")
public class MessageServiceResource extends AbstractResource {
    
    @Context
    private MessageService messageService;
    
    @Context
    private ResourceContext resourceContext;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public NMR getMessageService() throws ManagementException {
        return messageService.getNMR();
    }
    
    @GET
    @Path("stats")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Metric> getMessageServiceStatistics() throws ManagementException {
        return messageService.getStatistics();
    }
    
    @Path("endpoints")
    public EndpointsResource getEndpointsResource() {
        return resourceContext.initResource(new EndpointsResource());
    }
    
    @Path("{component}")
    public ComponentResource getComponentResource(@PathParam("component") String componentName) {
        return resourceContext.initResource(
                new ComponentResource(componentName));
    }
}
