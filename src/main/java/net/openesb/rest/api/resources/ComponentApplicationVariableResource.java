package net.openesb.rest.api.resources;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.ApplicationVariable;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentApplicationVariableResource extends AbstractResource {
    
    @Context
    private ConfigurationService configurationService;
    
    private final String componentName;
    
    public ComponentApplicationVariableResource(String componentName) {
        this.componentName = componentName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<ApplicationVariable> getApplicationVariables() throws ManagementException {
        Set<ApplicationVariable> variables = new TreeSet<ApplicationVariable>(new Comparator<ApplicationVariable>() {
            @Override
            public int compare(ApplicationVariable var1, ApplicationVariable var2) {
                return var1.getName().compareTo(var2.getName());
            }
        });

        variables.addAll(configurationService.
                getApplicationVariables(componentName));
        
        return variables;
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Set<ApplicationVariable> deleteApplicationVariables(@QueryParam("name") String name) throws ManagementException {
        configurationService.deleteApplicationVariables(componentName, new String []{name});
        
        return getApplicationVariables();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<ApplicationVariable> updateApplicationVariable(Set<ApplicationVariable> variables) throws ManagementException {
        configurationService.updateApplicationVariable(componentName, variables);
        
        return getApplicationVariables();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Set<ApplicationVariable> addApplicationVariable(Set<ApplicationVariable> variables) throws ManagementException {
        configurationService.addApplicationVariable(componentName, variables);
        
        return getApplicationVariables();
    }
}
