package net.openesb.rest.api.resources;

import java.util.Map;
import java.util.logging.Level;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.manage.Task;
import net.openesb.model.api.metric.Metric;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.ServiceAssemblyService;
import net.openesb.management.api.StatisticsService;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ServiceAssemblyResource extends AbstractResource {
    
    @Context
    private ServiceAssemblyService serviceAssemblyService;
    
    @Context
    private StatisticsService statisticsService;
    
    private final String assemblyName;
    
    public ServiceAssemblyResource(String assemblyName) {
        this.assemblyName = assemblyName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceAssembly getServiceAssembly() throws ManagementException {
        return serviceAssemblyService.getServiceAssembly(assemblyName);
    }
    
    @GET
    @Path("descriptor")
    @Produces(MediaType.APPLICATION_XML)
    public String getServiceAssemblyDescriptor(@QueryParam("su") String serviceUnit) throws ManagementException {
        return serviceAssemblyService.getDescriptorAsXml(assemblyName, serviceUnit);
    }
    
    @DELETE
    public Task undeploy(@DefaultValue("false") @QueryParam("force") boolean force) throws ManagementException {
        return serviceAssemblyService.undeploy(assemblyName, force);
    }
    
    @POST
    public Response doLifecycleAction(@QueryParam("action") LifecycleActionParam action,
            @DefaultValue("false") @QueryParam("force") boolean force) throws ManagementException {

        getLogger().log(Level.FINE, "Do lifecycle action {0} for service assembly {1}",
                new Object[]{action.getAction(), assemblyName});

        Task actionResult = null;
        
        switch (action.getAction()) {
            case START:
                actionResult = serviceAssemblyService.start(assemblyName);
                break;
            case STOP:
                actionResult = serviceAssemblyService.stop(assemblyName);
                break;
            case SHUTDOWN:
                actionResult = serviceAssemblyService.shutdown(assemblyName, force);
                break;
            default:
                getLogger().log(Level.WARNING, "Unknown action {0} for service assembly {1}",
                        new Object[]{action.getAction(), assemblyName});
                break;
        }
        
        if (actionResult == null) {
            return Response.serverError().build();
        } else {
            return Response.ok(actionResult, MediaType.APPLICATION_JSON).build();
        }
    }
    
    @GET
    @Path("stats")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Metric> getStatistics() throws ManagementException {
        return statisticsService.getServiceAssemblyStatistics(assemblyName);
    }
    
}
