package net.openesb.rest.api.resources;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import net.openesb.management.api.AdministrationService;
import net.openesb.rest.utils.FileUtils;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractResource {
    
    private static Logger sLogger = null;
    
    @Context
    private AdministrationService administrationService;
    
    /**
     * Get the logger for the REST services.
     *
     * @return java.util.Logger is the logger
     */
    protected Logger getLogger() {
        // late binding.
        if (sLogger == null) {
            sLogger = Logger.getLogger("net.openesb.rest.api");
        }
        return sLogger;
    }
    
    protected File createTemporaryFile(InputStream is, String fileName) {
        File uploadedFile = new File(
                administrationService.getTempStore(), fileName);
        FileUtils.writeToFile(is, uploadedFile);
        
        return uploadedFile;
    }
    
    public static String name(String name, String... names) {
        final StringBuilder builder = new StringBuilder();
        append(builder, name);
        if (names != null) {
            for (String s : names) {
                append(builder, s);
            }
        }
        return builder.toString();
    }
    
    private static void append(StringBuilder builder, String part) {
        if (part != null && !part.isEmpty()) {
            if (builder.length() > 0) {
                builder.append('.');
            }
            builder.append(part);
        }
    }
}
