package net.openesb.rest.api.resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.openesb.management.api.ManagementException;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.manage.Task;
import net.openesb.management.api.ServiceAssemblyService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("/assemblies")
public class ServiceAssembliesResource extends AbstractResource {

    @Context
    private ServiceAssemblyService serviceAssemblyService;
    
    @Context
    private ResourceContext resourceContext;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<ServiceAssembly> listServiceAssemblies(@QueryParam("state") String state,
            @QueryParam("component") String componentName) throws ManagementException {

        Set<ServiceAssembly> assemblies = new TreeSet<ServiceAssembly>(new Comparator<ServiceAssembly>() {
            @Override
            public int compare(ServiceAssembly ass1, ServiceAssembly ass2) {
                return ass1.getName().compareTo(ass2.getName());
            }
        });
        
        assemblies.addAll(serviceAssemblyService.findServiceAssemblies(
                    state,
                    componentName));
        
        return assemblies;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public Response deploy(@FormDataParam("assembly") InputStream is,
        @FormDataParam("assembly") FormDataContentDisposition fileDisposition) throws ManagementException {
        
        File assemblyArchive = null;
        
        try {
            assemblyArchive = createTemporaryFile(is, fileDisposition.getFileName());
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "I/O errors while uploading the assembly archive.", e);
            return Response.serverError().build();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    getLogger().log(Level.SEVERE, null, ex);
                }
            }
        }
        
        if (assemblyArchive != null) {
            Task actionResult = serviceAssemblyService.deploy(assemblyArchive.toURI().toString());
            return Response.ok(actionResult, MediaType.APPLICATION_JSON).build();
        }
        
        return Response.serverError().build();
    }

    @Path("{assembly}")
    public ServiceAssemblyResource getServiceAssemblyResource(@PathParam("assembly") String assemblyName) {
        return resourceContext.initResource(
                new ServiceAssemblyResource(assemblyName));
    }
}
