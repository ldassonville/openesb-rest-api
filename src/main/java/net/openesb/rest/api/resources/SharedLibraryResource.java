package net.openesb.rest.api.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import net.openesb.model.api.ComponentDescriptor;
import net.openesb.model.api.SharedLibrary;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.SharedLibraryService;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class SharedLibraryResource extends AbstractResource {
    
    @Context
    private SharedLibraryService sharedLibraryService;
    
    private final String sharedLibraryName;
    
    public SharedLibraryResource(String sharedLibraryName) {
        this.sharedLibraryName = sharedLibraryName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public SharedLibrary getSharedLibrary() throws ManagementException {
        return sharedLibraryService.getSharedLibrary(sharedLibraryName);
    }
    
    @GET
    @Path("descriptor")
    @Produces(MediaType.APPLICATION_JSON)
    public ComponentDescriptor getComponentDescriptor() throws ManagementException {
        return sharedLibraryService.getDescriptor(sharedLibraryName);
    }
    
    @GET
    @Path("descriptor")
    @Produces(MediaType.APPLICATION_XML)
    public String getComponentDescriptorAsXML() throws ManagementException {
        return sharedLibraryService.getDescriptorAsXml(sharedLibraryName);
    }
    
    @DELETE
    public Response uninstall() throws ManagementException {
        sharedLibraryService.uninstall(sharedLibraryName);
        
        return Response.ok().build();
    }
}
