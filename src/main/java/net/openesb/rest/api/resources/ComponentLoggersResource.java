package net.openesb.rest.api.resources;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.Logger;
import net.openesb.management.api.ComponentService;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentLoggersResource extends AbstractResource {
    
    @Context
    private ComponentService componentService;
    
    private final String componentName;
    
    public ComponentLoggersResource(String componentName) {
        this.componentName = componentName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Logger> getLoggers() throws ManagementException {
        Set<Logger> loggers = new TreeSet<Logger>(new Comparator <Logger>() {
                @Override
                public int compare(Logger log1, Logger log2) {
                    return log1.getName().compareTo(log2.getName());
                }
            });
        
        loggers.addAll(componentService.getLoggers(componentName));
        
        return loggers;
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Logger> setLevel(@QueryParam("logger") String loggerName, @QueryParam("level") LoggerLevelParam level) throws ManagementException {
        componentService.setLoggerLevel(componentName, loggerName, level.getLevel());
        
        // Return the list of level
        return getLoggers();
    }
}
