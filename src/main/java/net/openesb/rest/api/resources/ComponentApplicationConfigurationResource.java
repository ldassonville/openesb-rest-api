package net.openesb.rest.api.resources;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.ApplicationConfiguration;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.ManagementException;
import net.openesb.model.api.JBIComponent;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentApplicationConfigurationResource extends AbstractResource {
    
    @Context
    private ConfigurationService configurationService;
    
    private final String componentName;
    
    public ComponentApplicationConfigurationResource(String componentName) {
        this.componentName = componentName;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<String> getApplicationConfigurations() throws ManagementException {
        return new TreeSet<String>(
                configurationService.getApplicationConfigurations(componentName));
    }
    
    @GET
    @Path("{applicationConfigurationName}")
    @Produces(MediaType.APPLICATION_JSON)
    public ApplicationConfiguration getApplicationConfiguration(@PathParam("applicationConfigurationName") String applicationConfigurationName) throws ManagementException {
        return configurationService.getApplicationConfiguration(componentName, applicationConfigurationName);
    }
    
    @DELETE
    @Path("{applicationConfigurationName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<String> deleteApplicationConfiguration(@PathParam("applicationConfigurationName") String applicationConfigurationName) throws ManagementException {
        configurationService.deleteApplicationConfiguration(componentName, applicationConfigurationName);
        
        return getApplicationConfigurations();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateApplicationConfiguration(ApplicationConfiguration applicationConfiguration) throws ManagementException {
        configurationService.updateApplicationConfiguration(componentName, applicationConfiguration);
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void addApplicationConfiguration(ApplicationConfiguration applicationConfiguration) throws ManagementException {
        configurationService.addApplicationConfiguration(componentName, applicationConfiguration);
    }
}
