package net.openesb.rest.api.resources;

import javax.ws.rs.Path;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ComponentApplicationResource {
    
    @Context
    private ResourceContext resourceContext;
    private final String componentName;

    public ComponentApplicationResource(String componentName) {
        this.componentName = componentName;
    }
    
    @Path("variables")
    public ComponentApplicationVariableResource getComponentApplicationVariableResource() {
        return resourceContext.initResource(
                new ComponentApplicationVariableResource(componentName));
    }
    
    @Path("configurations")
    public ComponentApplicationConfigurationResource getComponentApplicationConfigurationResource() {
        return resourceContext.initResource(
                new ComponentApplicationConfigurationResource(componentName));
    }
}
