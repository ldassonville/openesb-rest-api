package net.openesb.rest.api.provider;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
//@Provider
public class BasicAuthenticationFilter implements ContainerRequestFilter {

//    @Context
//    private EnvironmentContext context;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Automatically allow certain requests.
        /*
         String method = requestContext.getMethod();
         String path = requestContext.getPath(true);
        
         //We do allow wadl to be retrieve
         if(method.equals("GET") && path.equals("application.wadl")){
         return requestContext;
         }*/

        //Get the authentification passed in HTTP headers parameters
        String auth = requestContext.getHeaderString("authorization");

        //If the user does not have the right (does not provide any HTTP Basic Auth)
        if (auth == null) {
            requestContext.abortWith(Response
                    .status(Response.Status.UNAUTHORIZED)
                    .entity("User cannot access the resource.")
                    .build());
        } else {
            //lap : loginAndPassword
            String[] lap = decode(auth);

            //If login or password fail
            if (lap == null || lap.length != 2) {
                requestContext.abortWith(Response
                        .status(Response.Status.UNAUTHORIZED)
                        .entity("User cannot access the resource.")
                        .build());
            }

            /*
            try {
                context.getPlatformContext().getSecurityProvider().login(
                        new UsernamePasswordToken(lap[0], lap[1]));
            } catch (AuthenticationException ae) {
                requestContext.abortWith(Response
                        .status(Response.Status.UNAUTHORIZED)
                        .entity("User " + lap[0] + " cannot access the resource.")
                        .build());
            }*/
        }
    }

    /**
     * Decode the basic auth and convert it to array login/password
     *
     * @param auth The string encoded authentification
     * @return The login (case 0), the password (case 1)
     */
    public static String[] decode(String auth) {
        //Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
        auth = auth.replaceFirst("[B|b]asic ", "");

        //Decode the Base64 into byte[]
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

        //If the decode fails in any case
        if (decodedBytes == null || decodedBytes.length == 0) {
            return null;
        }

        //Now we can convert the byte[] into a splitted array :
        //  - the first one is login,
        //  - the second one password
        return new String(decodedBytes).split(":", 2);
    }
}
