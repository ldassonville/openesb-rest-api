package net.openesb.rest.api.provider;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 * Allow the system to serve xhr level 2 from all cross domain site
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
public class CrossDomainFilter implements ContainerResponseFilter {

    /**
     * Add the cross domain data to the output if needed.
     * 
     * @param reqCtx The container request (input)
     * @param respCtx The container request (output)
     * @throws IOException 
     */
    @Override
    public void filter(ContainerRequestContext reqCtx, ContainerResponseContext respCtx) throws IOException {
        respCtx.getHeaders().add("Access-Control-Allow-Origin", "*");
        respCtx.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        respCtx.getHeaders().add("Access-Control-Allow-Credentials", "true");
        respCtx.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        respCtx.getHeaders().add("Access-Control-Max-Age", "1209600");
    }   
}
