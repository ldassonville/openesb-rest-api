package net.openesb.rest.api.provider;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import net.openesb.management.api.ManagementException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
public class ManagementExceptionMapper implements ExceptionMapper<ManagementException> {

    @Override
    public Response toResponse(ManagementException me) {
        return Response.serverError().entity(me.getMessage()).build();
    }
}
